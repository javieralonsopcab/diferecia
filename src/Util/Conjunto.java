/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

/**
 * Clase contenedora: Cada uno de sus elementos con cajas parametrizadas
 * Conjunto es una estructura da datos estática
 *
 * @author MADARME
 * @param <T>
 */
public class Conjunto<T> {

    //Estructura de datos estática
    private Caja<T>[] cajas;
    private int i = 0;

    public Conjunto() {
    }

    public Conjunto(int cantidadCajas) {
        if (cantidadCajas <= 0) {
            throw new RuntimeException("No se pueden crear el Conjunto");
        }

        this.cajas = new Caja[cantidadCajas];
    }

    public void adicionarElemento(T nuevo) throws Exception {
        if (i >= this.cajas.length) {
            throw new Exception("No hay espacio en el Conjunto");
        }

        if (this.existeElemento(nuevo)) {
            throw new Exception("No se puede realizar inserción, elemento repetido");
        }

        this.cajas[i] = new Caja(nuevo);
        this.i++;

    }

    public T get(int indice) {
        if (indice < 0 || indice >= this.getLength()) {
            throw new RuntimeException("Índice fuera de rango");
        }

        return this.cajas[indice].getObjeto();

    }
    
    /*
    Este metodo calcula la diferencia entre el conjunto A y el conjunto B
    */
    public Conjunto<T> getDiferencia(Conjunto<T> c2) throws Exception{
        int serepitieron = this.elementosRepetidos(c2);
        Conjunto<T> resultado = new Conjunto<>(this.getCapacidad()-serepitieron);
        for (Caja<T> caja : this.cajas) {
            if (!c2.existeElemento(caja.getObjeto())) {
                resultado.adicionarElemento(caja.getObjeto());
            }
        }
        return resultado;
    }

    public int indexOf(T objBuscar) {

        for (int j = 0; j < i; j++) {

            //Sacando el estudiante de la caja:
            T x = this.cajas[j].getObjeto();

            if (x.equals(objBuscar)) {
                return j;
            }
        }

        return -1;

    }

    public void set(int indice, T nuevo) {
        if (indice < 0 || indice >= this.getLength()) {
            throw new RuntimeException("Índice fuera de rango");
        }

        this.cajas[indice].setObjeto(nuevo);

    }

    public boolean existeElemento(T nuevo) {

        //Sólo estoy comparando por los estudiantes matriculados
        for (int j = 0; j < i; j++) {

            //Sacando el estudiante de la caja:
            T x = this.cajas[j].getObjeto();

            if (x.equals(nuevo)) {
                return true;
            }
        }

        return false;

    }

    /**
     * para el grupo A--> Selección para el grupo C--> Inserción
     *
     */
    public void ordenar() {
        // :) ordenamiento por insercion
        int posicion=0;
        for(int i=1; i<this.i; i++){

            T aux=this.cajas[i].getObjeto();
            posicion=i;
            Comparable comparador = (Comparable) aux;

            for(int contador = posicion-1; contador>=0 && comparador.compareTo(this.get(contador))<0; contador--){
                this.cajas[contador+1].setObjeto(this.cajas[contador].getObjeto());
                this.cajas[contador].setObjeto(aux);
            }
            
        }

    }

    /**
     * Realiza el ordenamiento por burbuja
     */
    public void ordenarBurbuja() {
        for (int i = 0; i < this.i - 1; i++) {
            for (int j = 0; j < this.i - 1; j++) {
                T objeto = this.cajas[j].getObjeto();
                Comparable comparar = (Comparable) objeto;
                T otroObjeto = this.cajas[j+1].getObjeto();
                int rta = comparar.compareTo(otroObjeto);
                if (rta > 0) {
                    T tmp = this.cajas[j].getObjeto();
                    this.cajas[j].setObjeto(this.cajas[j + 1].getObjeto());
                    this.cajas[j + 1].setObjeto(tmp);
                }
            }
        }
    }

    /**
     * Elimina un elemento del conjunto y compacta
     *
     * @param objBorrado es el objeto que deseo eliminar
     */
    public void remover(T objBorrado) {
        int pocision = this.indexOf(objBorrado);
        this.cajas[pocision] = null;
        for (int i = pocision; i < this.i - 1; i++) {
            this.cajas[i] = this.cajas[i + 1];
        }
        this.cajas[this.i-1] = null;
        this.i--;
    }

    /**
     * El método adiciona todos los elementos de nuevo en el conjunto
     * original(this) y el nuevo queda vacío. En este proceso no se toma en
     * cuenta los datos repetidos Ejemplo: conjunto1=<1,2,3,5,6> y
     * conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2) da como resultado:
     * conjunto1=<1,2,3,5,6,9,1,8> y conjunto2=null
     *
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     * @throws java.lang.Exception
     */
   public void concatenar(Conjunto<T> nuevo) throws Exception {
        int tamanio = this.getCapacidad() + nuevo.getCapacidad();
        Conjunto<T> nuevoConjunto = new Conjunto<>(tamanio);
        int i = 0;
        int j = this.getCapacidad();
        int k=0;
        int aux = this.getCapacidad();
        while (i < this.getCapacidad()) {
            nuevoConjunto.adicionarElemento(this.get(i));
            i++;
        }
        while (j < tamanio) {
            if (nuevoConjunto.existeElemento(nuevo.cajas[k].getObjeto())) {
                int x = nuevoConjunto.getCapacidad();
                T nuevot = nuevo.cajas[k].getObjeto();
                nuevoConjunto.cajas[x] = new Caja(nuevot);
                nuevoConjunto.i++;
                j++;
                k++;
            } else {
                nuevoConjunto.adicionarElemento(nuevo.get(k));
                k++;
                j++;
            }
        }
        this.cajas = nuevoConjunto.cajas;
        this.i = nuevoConjunto.getCapacidad();
        nuevo.removeAll();
    }
    
    /**
     * El método adiciona todos los elementos de nuevo en el conjunto
     * original(this) y el nuevo queda vacío. En este proceso SI toma en cuenta
     * los datos repetidos Ejemplo: conjunto1=<1,2,3,5,6> y conjunto2=<9,1,8>
     * conjunto1.concatenar(conjunto2) da como resultado:
     * conjunto1=<1,2,3,5,6,9,8> y conjunto2=null
     *
     * @param nuevo el objeto conjunto a ser colocado en el conjunto original
     */
    public void concatenarRestrictivo(Conjunto<T> nuevo) throws Exception {
        int tamanio = this.getCapacidad() + nuevo.getCapacidad();
        int repetidos = this.elementosRepetidos(nuevo);
        Conjunto<T> nuevoConjunto = new Conjunto<>(tamanio-repetidos);
        int i = 0;
        int j = this.getCapacidad();
        int k=0;
        while (i < this.getCapacidad()) {
            nuevoConjunto.cajas[i] = new Caja(this.cajas[i].getObjeto());
            i++;
        }
        nuevoConjunto.i = this.getCapacidad();
        while (j < tamanio) {
            if (!nuevoConjunto.existeElemento(nuevo.get(k))) {
                nuevoConjunto.adicionarElemento(nuevo.get(k));
                j++;
                k++;
            } else {
                j++;
                k++;
            }
        }
        this.cajas = nuevoConjunto.cajas;
        this.i = nuevoConjunto.getCapacidad();
        nuevo.removeAll();
    }
    
    /**
     * El metodo retorna la cantidad de elementos repetidos para que al crear un nuevo conjunto este no quede con espacios null
     * e innecesarios de aquellos que se repitieron
     * @param Elconjunto2 para evaluarse en el conjunto1
     * @return repetidos
     */
    private int elementosRepetidos(Conjunto <T> copia){
        int repetidos=0;
        for(int i=0; i<copia.getCapacidad(); i++){
            if(this.existeElemento(copia.cajas[i].getObjeto()))
                repetidos++;
        }
        return repetidos;
    }

    public void removeAll() {
        this.cajas = null;
        this.i = 0;
    }

    @Override
    public String toString() {
        String msg = "******** CONJUNTO*********\n";

        for (Caja c : this.cajas) {
            if(c!=null)
            msg += c.getObjeto().toString() + "\n";
        }

        return msg;
    }

    /**
     * Obtiene la cantidad de elementos almacenados
     *
     * @return retorna un entero con la cantidad de elementos
     */
    public int getCapacidad() {
        return this.i;
    }

    /**
     * Obtiene el tamaño máximo de cajas dentro del Conjunto
     *
     * @return int con la cantidad de cajas
     */
    public int getLength() {
        return this.cajas.length;
    }

    /**
     * Obtiene el mayor elemento del Conjunto, recordar que el conjunto no posee
     * elementos repetidos.
     *
     * @return el elemnto mayor de la colección
     */
    public T getMayorElemento() {
        if (this.cajas == null) {
            throw new RuntimeException("No se puede encontrar elemento mayor, el conjunto está vacío");
        }

        T mayor = this.cajas[0].getObjeto();
        for (int i = 1; i < this.getCapacidad(); i++) {
            //Utilizo la interfaz comparable y después su método compareTo
            Comparable comparador = (Comparable) mayor;
            T dato_A_comparar = this.cajas[i].getObjeto();
            // Resta entra mayor-datoAComparar 
            int rta_compareTo = comparador.compareTo(dato_A_comparar);
            if (rta_compareTo < 0) {
                mayor = dato_A_comparar;
            }
        }
        return mayor;
    }
}
