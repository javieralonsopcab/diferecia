/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Persona;
import Util.Conjunto;

/**
 *
 * @author Javier
 */
public class TestDiferenciaConjunto {
    
    public static void main(String[] args) {
        try{
        Conjunto<Persona> myConjunto1 = new Conjunto<>(5);
        Persona p1 = new Persona("Freddie Mercury",true,45,1.77F);
        myConjunto1.adicionarElemento(p1);
        Persona p2 = new Persona("Javier Parada",true,18,1.75F);
        myConjunto1.adicionarElemento(p2);
        Persona p3 = new Persona("Cristiano Ronaldo",true,35,1.87F);
        myConjunto1.adicionarElemento(p3);
        Persona p4 = new Persona("Lionel Messi",true,32,1.7F);
        myConjunto1.adicionarElemento(p4);
        Persona p5 = new Persona("Shakira",false,43,1.57F);
        myConjunto1.adicionarElemento(p5);
            System.out.println("Este es el conjunto A: \n"+myConjunto1.toString());
        
        
        Conjunto<Persona> myConjunto2 = new Conjunto<>(5);
        Persona p01 = new Persona("Jennifer Lopez",false,50,1.64F);
        myConjunto2.adicionarElemento(p01);
        Persona p02 = new Persona("Ernesto che Guevara",true,39,1.75F);
        myConjunto2.adicionarElemento(p02);
        Persona p011 = new Persona("Freddie Mercury",true,45,1.77F);
        myConjunto2.adicionarElemento(p011);
        Persona p03 = new Persona("Fidel Castro",true,90,1.91F);
        myConjunto2.adicionarElemento(p03);
        Persona p05 = new Persona("Shakira",false,43,1.57F);
        myConjunto2.adicionarElemento(p05);
            System.out.println("Este es el conjunto B: \n"+myConjunto2.toString());
        
            System.out.println("La diferencia del conjunto A y el conjunto B es: \n");
            System.out.println(myConjunto1.getDiferencia(myConjunto2));
        
        
        }catch(Exception e){
            System.err.println(e.getMessage());
        }
        
        
    }
}
