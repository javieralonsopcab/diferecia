/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 *
 * @author Javier
 */
public class Persona {
    
    private String nombre;
    private boolean sexo; //false mujer, true hombre
    private int edad;
    private float estatura;

    public Persona(String nombre, boolean sexo, int edad, float estatura) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.edad = edad;
        this.estatura = estatura;
    }

    @Override
    public boolean equals(Object persona){
      Persona p = (Persona)persona;
      return this.nombre.equals(p.nombre) && this.sexo == p.sexo && 
              this.edad == p.edad && this.estatura==p.estatura;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public boolean isSexo() {
        return sexo;
    }

    public void setSexo(boolean sexo) {
        this.sexo = sexo;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public float getEstatura() {
        return estatura;
    }

    public void setEstatura(float estatura) {
        this.estatura = estatura;
    }
    
    public String toString(){
        return "Nombre: "+this.nombre+", Es Hombre: "+this.sexo+ ", Edad: "+this.edad+", Estatura: "+this.estatura;
    }
}
